
    <div class="container-fluid pl-0" style="background-color: gainsboro; margin-top: 100px ">
        <div class="row text-left">
            <div class="col-md-5 col-md-5">
                <h1 class="text-dark" style="text-align: center">Despre</h1>
                <p class="text-dark">Acest site a fost creat de un student incepator indrumat de un profesor coordonator.</p>
                <p class="text-dark">Copyright ©2019-2020 All rights reserved </p>
            </div>
            <div class="col-md-4">
                <h4 >Noutati</h4>
                <form class="form-inline">
                    <div class="col-pd-0">
                        <div class="input-group pr-5">
                            <input type="text" class="form-control bg-dark text-white" placeholder="Email">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span>&#8594;</span>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
            <div class="col-md-3 col-sm-12">
                <h4 class="text-dark">Urmareste-ne</h4>
                <div class="column">
                    <a href="https://www.facebook.com/" class="fa fa-facebook"></a>
                    <a href="https://twitter.com/login?lang=ro" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-instagram"></a>
                    <a href="#" class="fa fa-youtube"></a>
                </div>
            </div>
        </div>
    </div>
