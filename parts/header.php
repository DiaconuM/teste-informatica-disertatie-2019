<div class="container-fluid p-0" style="background-color: #ff512f">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="../bootstrap/index.php">Sa invatam exersand</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <div class="mr-auto"></div>
            <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="listTests.php">Invata</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="despre.php">Despre</a>
                </li>
                <li class="nav-item dropdown">
                    <div  class="dropdown">
                        <a class="nav-link" href="rezultate.php" tabindex="-1" aria-disabled="true">Contul meu</a>
                        <div class="dropdown-content">
                            <a href="#">Clasament</a>
                            <a href="#">Punctaj</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>