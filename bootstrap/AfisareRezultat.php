<!Doctype html>
<html lang="en">
<head>
    <?php include "../parts/head.php"; ?>
</head>
<header>
    <?php include "../parts/header.php"; ?>
</header>
<body>
<!--Continut-->
<div class="container-fluid">
<div class="row">
    <div class="col-9">
        <h2>Punctaj</h2>


        <div class="tab">
            <button class="tablinks" onmouseover="openCity(event, 'London')">Level 1</button>
            <button class="tablinks" onmouseover="openCity(event, 'Paris')">Level 1</button>
            <button class="tablinks" onmouseover="openCity(event, 'Tokyo')">Level 1</button>
        </div>

        <div id="London" class="tabcontent">
            <h3>Level 1</h3>
            <p>Ai obtinut punctajul X.</p>
        </div>

        <div id="Paris" class="tabcontent">
            <h3>Level 2</h3>
            <p>Ai obtinut punctajul Y.</p>
        </div>

        <div id="Tokyo" class="tabcontent">
            <h3>Level 3 </h3>
            <p>Ai obtinut punctazul Z.</p>
        </div>

        <div class="clearfix"></div>

        <script>
            function openCity(evt, cityName) {
                var i, tabcontent, tablinks;
                tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablinks");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].className = tablinks[i].className.replace(" active", "");
                }
                document.getElementById(cityName).style.display = "block";
                evt.currentTarget.className += " active";
            }
        </script>



    </div>
<div class="col-3 col-sm-9 col-md-3">
        <form class="form-container">
<h2>Clasament</h2>
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Score</th>

        </tr>
        </thead>
        <tbody>
        <tr>
            <td>John</td>
            <td>20</td>

        </tr>
        <tr>
            <td>Mary</td>
            <td>50</td>

        </tr>
        <tr>
            <td>July</td>
            <td>10</td>

        </tr>
        </tbody>
    </table>

    </div></div>


<footer>
    <?php include "../parts/footer.php"; ?>
</footer>


</div>




</body>
</html>