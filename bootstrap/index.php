<!DOCTYPE html>
<html lang="en">
<head>
<?php include "../parts/head.php" ?>

</head>
<body>
<!--Navbar1-->
<header style="background-color: #ff512f">
    <div class="container-fluid p-0">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="index.php">Sa invatam exersand</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <div class="mr-auto"></div>
            <ul class="navbar-nav">

                <li class="nav-item ">
                    <div  class="dropdown">
                    <a class="nav-link" href="login.php" tabindex="-1" aria-disabled="true">Conecteaza-te</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    </div>
</header>
    <!--Continut-->
    <div class="container-fluid text-center">
<div class="row">
    <div class="col-md-9 col-sm-12">
        <h6 style="margin-top: 80px">Testeaza-ti cunostintele</h6>
        <a href="login.php" > <button type="button" class="btn btn-success px-5 py-2">Incepe</button></a>
        <br>
        <a href="simpleLogin.php"><button type="button" class="btn btn-primary px-5 py-2">Am deja un cont</button></a>
    </div>
    <div class="col-md-3 col-sm-12 h-25">
        <img src="../picture/book.JPG" >
    </div>
</div>

    </div>
<main>
 <section class="section-1">
     <div class="container text-center">
         <div class="row">
             
         </div>
     </div>
 </section>
</main>

<!--Footer-->
<?php include "../parts/footer.php"?>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

</body>
</html>