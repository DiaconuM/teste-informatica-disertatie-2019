
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <?php include "../parts/head.php" ?>

</head>
<body>
<header><?php include "../parts/header.php"?></header>

    <!--Continut-->

    <div class="container-fluid bg-center">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-3">
                    <form class="form-container">
                        <h2>Creeaza cont</h2>
                        <div class="form-group">
                            <label for="name">Nume:</label>
                            <input type="name" class="form-control" id="name" placeholder="Enter name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Inregistreaza-te</button>
                    </form>
                </div>
            <div class="col-12 col-sm-6 col-md-3">
                    <form class="form-container">
                        <h2>Login</h2>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email </label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1"> Check me out </label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block ">Login</button>
                    </form>

                </div>
            </div>

        </div>

</body>
</html>