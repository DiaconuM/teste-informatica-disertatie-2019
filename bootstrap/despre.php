<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <?php include "../parts/head.php" ?>

</head>
<body>
<header><?php include "../parts/header.php"?></header>

<div class="container" style="background-color: transparent">
    <div class="row">
        <h1 style="text-color:black"> Modul de generare automata de grille bazat pe istoricul utilizatorului  </h1>
        <p style="background-color: gold">
            Aceasta aplicatie a fost conceputa pentru a testa cunostiintele generale despre notiuni informatice ale unui utilizator prin testare.
            Este o aplicatie Web ce foloseste ca si tehnologie limbajul de programare PHP, framework-ul Bootstrap si CSS cat si baza de date mysql.
            Aceste 3 tehnologii fac ca aceasta aplicatie sa functioneze.
        </p>
        <h3 style="text-color: black">PHP</h3>
        <p style="background-color: #ff512f"> PHP însemna inițial Personal Home Page.PHP a fost început în 1994 ca o extensie a limbajului server-side Perl, și apoi ca o serie de CGI-uri compilate de către Rasmus Lerdorf, pentru a genera un curriculum vitae și pentru a urmări numărul de vizitatori ai unui site. Apoi a evoluat în PHP/FI 2.0, dar proiectul open-source a început să ia amploare după ce Zeev Suraski și Andi Gutmans, de la Technion au lansat o nouă versiune a interpretorului PHP în vara anului 1998, această versiune primind numele de PHP 3.0. Tot ei au schimbat și numele în acronimul recursiv de acum, până atunci PHP fiind cunoscut ca Personal Home Page Tools. Apoi Suraski și Gutmans au rescris baza limbajului, producând astfel și Zend Engine în 1999. În mai 2000 a fost lansat PHP 4.0, având la bază Zend Engine 1.0.
            PHP este simplu de utilizat, fiind un limbaj de programare structurat, ca și C-ul, Perl-ul sau începând de la versiunea 5 chiar Java, sintaxa limbajului fiind o combinație a celor trei. Datorită modularității sale poate fi folosit și pentru a dezvolta aplicații de sine stătătorare, de exemplu în combinație cu PHP-GTK sau poate fi folosit ca Perl sau Python în linia de comandă. Probabil una din cele mai importante facilități ale limbajului este conlucrarea cu majoritatea bazelor de date relaționale, de la MySQL și până la Oracle, trecând prin MS Sql Server, PostgreSQL, sau DB2.
            PHP poate rula pe majoritatea sistemelor de operare, de la UNIX, Windows, sau Mac OS X și poate interacționa cu majoritatea serverelor web. Codul dumneavoastră PHP este interpretat de serverul WEB și generează un cod HTML care va fi văzut de utilizator (clientului -browserului- fiindu-i transmis numai cod HTML).
            Arhitectura tip LAMP a devenit populară în industria web ca modalitate rapidă, gratuită și integrată de dezvoltare a aplicațiilor. Alături de Linux, Apache și Mysql, PHP reprezintă litera P, deși uneori aceasta se referă la Python sau Perl. Linux ocupă rolul de sistem de operare pentru toate celelalte aplicații, MySQL gestionează bazele de date, Apache are rol de server web, iar PHP are rol de interpretator și comunicator între acestea.
            PHP foloseste extensii specifice pentru fișierele sale: .php, .php3, .ph3, .php4, .inc, .phtml. Aceste fișiere sunt interpretate de catre serverul web iar rezultatul este trimis în formă de text sau cod HTML către browser-ul clientului.
        </p>
        <h3 style="text-color: black"> Bootstrap</h3>
        <p style="background-color: papayawhip">
            Bootstrap este cel de-al treilea proiect cu cea mai mare vedetă de pe GitHub, cu peste 131.000 de stele, în spatele doar freeCodeCamp (aproape 300.000 de stele) și marginal în spatele cadrului Vue.js.  Conform Alexa Rank, Bootstrap getbootstrap.com se află în top-2000 în SUA, în timp ce vuejs.org este în top 7000 în SUA
            Bootstrap, numit inițial Twitter Blueprint, a fost dezvoltat de Mark Otto și Jacob Thornton la Twitter ca cadru pentru a încuraja coerența între instrumentele interne. Înainte de Bootstrap, diverse biblioteci au fost utilizate pentru dezvoltarea interfeței, ceea ce a dus la incoerențe și o sarcină mare de întreținere. Potrivit dezvoltatorului Twitter Mark Otto:
            "Un grup foarte mic de dezvoltatori și ne-am reunit pentru a proiecta și a construi un nou instrument intern și am văzut oportunitatea de a face ceva mai mult. Prin acest proces, ne-am văzut construind ceva mult mai substanțial decât un alt instrument intern. Lună mai târziu, am încheiat cu o versiune timpurie a Bootstrap ca o modalitate de a documenta și a împărtăși modele comune de proiectare și active în cadrul companiei. "
            Bootstrap este un cadru web care se concentrează pe simplificarea dezvoltării paginilor web informative (spre deosebire de aplicațiile web). Scopul principal al adăugării la un proiect web este de a aplica opțiunile Bootstrap de culoare, dimensiune, font și aspect la acel proiect. Ca atare, factorul principal este dacă dezvoltatorii responsabili găsesc acele alegeri după bunul lor plac. Odată adăugat la un proiect, Bootstrap oferă definiții de bază ale stilului pentru toate elementele HTML. Rezultatul final este un aspect uniform pentru proză, tabele și elemente de formă din browserele web. În plus, dezvoltatorii pot profita de clasele CSS definite în Bootstrap pentru a personaliza în continuare aspectul conținutului lor. De exemplu, Bootstrap a prevăzut tabele cu culori deschise și închise, titluri de pagină, citate de tragere mai proeminente și text cu evidențiere.
            Bootstrap vine de asemenea cu mai multe componente JavaScript sub formă de plugin-uri jQuery. Acestea furnizează elemente suplimentare de interfață pentru utilizator, cum ar fi casetele de dialog, fișele de unelte și caruselele. Fiecare componentă Bootstrap constă dintr-o structură HTML, declarații CSS și, în unele cazuri, însoțitoare de cod JavaScript. De asemenea, extind funcționalitatea unor elemente de interfață existente, inclusiv de exemplu o funcție automată completă pentru câmpurile de intrare.

        </p>
        <h3>MySQL</h3>
        <p style="background-color: bisque">
            Ansgar Becker a început dezvoltarea pe un front MySQL în 1999, denumind proiectul „MySQL-Front” și a folosit un strat de API direct scris de Matthias Fichtner pentru a interfața cu serverele MySQL și bazele de date conținute. Dezvoltarea privată a continuat până în 2003 cu versiunea 2.5.
            În 2004, într-o perioadă de inactivitate, Becker a vândut marca MySQL-Front către Nils Hoyer, care a continuat dezvoltarea prin clonarea software-ului original.
            În aprilie 2006, Becker a deschis aplicația pe SourceForge, redenumind proiectul „HeidiSQL”. HeidiSQL a fost re-proiectat pentru a utiliza un strat de interfață de date mai nou și mai popular, ZeosLib, care a debutat în versiunea 3.0.
            Stratul bazei de date a fost din nou înlocuit de o abordare cu o singură unitate în octombrie 2009 de către Becker. Ulterior, aceasta a fost extinsă din nou pentru sprijinirea altor servere de baze de date.
            Suportul pentru Microsoft SQL Server a fost adăugat în martie 2011 pentru versiunea 7.0.
            De la lansarea 8.0, HeidiSQL își oferă GUI în aproximativ 22 de limbi, altele decât engleza. Traducerile sunt contribuite de utilizatori din diferite țări prin intermediul Transifex.
            Suportul PostgreSQL a fost introdus în martie 2014 pentru versiunea 9.0.
            La începutul anului 2018, o versiune ușoară extinsă a v9 .5 a fost publicată pe Microsoft Store.

        </p>

    </div>
</div>
<?php include "../parts/footer.php" ?>