<?php
include  "checkLogin.php";

$session = new Session();
$session->user_id = $user->getId();
$questionId = key($_POST['answer']);
$question = Question::find($questionId);
$session->date = date('Y-m-d h:i:s');
$session->test_id = $question->test->getId();
$session->save();
foreach ($_POST['answer'] as $questionId => $answerId){
    $userAnswer = new UserAnswer();
    $userAnswer->session_id = $session->getId();
    $userAnswer->answer_id = $answerId;
    $userAnswer->question_id = $questionId;
    $userAnswer->user_id = $user->getId();
    $userAnswer->save();
}
header('Location: listTests.php');