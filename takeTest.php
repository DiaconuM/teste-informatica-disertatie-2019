
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <?php include "parts/head.php" ?>

</head>
<body>
<header><?php include "parts/header.php"?></header>

<?php
include  "checkLogin.php";
?>
<form action="saveTest.php" method="post">
<?php
/** @var Test $test */
$test = Test::find($_GET['id']);
echo "<ol>";
/** @var Question $question */
foreach ($test->questions as $question) {
    echo "<li>{$question->quiz}";
    /** @var Answer $answer */
    echo "<ol type='a'>";
    foreach ($question->answers as $answer) {
        echo "<li><input type='radio' name='answer[{$question->getId()}]' value='{$answer->getId()}' />{$answer->answer}</li>";
    }
    echo "</ol>";
    echo "<br /></li>";
}
echo "</ol>";
?>
    <input type="submit" value="Salveaza" />
</form>
</body>
</html>