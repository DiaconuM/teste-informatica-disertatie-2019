<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<!-- head-->
<head>
    <?php include "../parts/head.php" ?>
</head>
<body >
    <header>
        <?php include "../parts/header.php"?>
    </header>
<!--Continut-->
<div class="container-fluid" >
    <?php include "../includes/functions.php";?>

    <div class="row">
        <div class="col-3">
                <div class="vertical-menu">
                    <a href="#" class="active">Menu</a>
                    <a href="#">quiz</a>

                </div>
            </div>
        <div class="col-9">
            <?php $products = quiz::findAll();?>
            <?php $columns = get_object_vars($products[0]); ?>
            <table class="table table-hover">
                <thead>
                <tr>
                    <?php foreach ($columns as $column => $value): ?>
                        <th scope="col"><?php echo $column; ?></th>
                    <?php endforeach; ?>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <?php foreach (get_object_vars($product) as $column => $value): ?>
                            <td><?php echo $value; ?></td>
                        <?php endforeach; ?>
                        <td>
                            <a href="../bootstrap/edit.php?id=<?php echo $product->getId(); ?>">Edit</a>
                            <a href="../bootstrap/delete.php?id=<?php echo $product->getId(); ?>">Delete</a>
                        </td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<footer>
    <?php include "../parts/footer.php"?>
</footer>
</body>
</html>