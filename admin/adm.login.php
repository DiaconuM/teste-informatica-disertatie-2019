<!Doctype html>
<html lang="en">
<head>
    <?php include "../parts/head.php"; ?>
</head>
<header>
    <?php include "../parts/header.php"; ?>
</header>
<body>
<!--Continut-->

    <div class="container-fluid bg-center">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-3">
                <form class="form-container">
                    <h2>Creeaza cont</h2>
                    <div class="form-group">
                        <label for="name">Nume:</label>
                        <input type="name" class="form-control" id="name" placeholder="Enter name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Inregistreaza-te</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>