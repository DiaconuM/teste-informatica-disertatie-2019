<?php
require_once "BaseEntity.php";
/**
 * Created by PhpStorm.
 * User: Mihaela
 * Date: 03/09/2019
 * Time: 19:25
 */
 class Session extends BaseEntity
{
    public $date;

    public $test_id;

    public $user_id;

     public function getRelations()
     {
         return [
             'test' => [
                 'target'=>Test::class,
                 'type' => self::MANY_TO_ONE,
                 'link' => 'test_id'
             ],
             'user' => [
                 'target'=>User::class,
                 'type' => self::MANY_TO_ONE,
                 'link' => 'user_id'
             ],
             'userAnswers' => [
                 'target'=>UserAnswer::class,
                 'type' => self::ONE_TO_MANY,
                 'link' => 'session_id'
             ],

         ];
     }
 }