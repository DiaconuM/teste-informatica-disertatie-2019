<?php
require_once "BaseEntity.php";
/**
 * Created by PhpStorm.
 * User: Mihaela
 * Date: 03/09/2019
 * Time: 19:28
 */
class User extends BaseEntity
{

public $name;

public $email;

public $password;


    public function getRelations()
    {
        return [
            'sessions' =>[
                'target'=>Session::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'user_id'
            ],
            'parent' => [
                'target'=>Answer::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'parent_id'
            ],

            'children' => [
                'target'=>User_answer::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'parent_id'
            ],

        ];
    }
}