<?php

abstract class BaseEntity
{
    const ONE_TO_MANY='oneToMany';
    const MANY_TO_ONE='manyToOne';

    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    abstract public function getRelations();

    public function __construct($id=null)
    {
        if (!is_null($id)) {
            $data = dbSelectOne($this->getTable(), ['id' => $id]);
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    function __get($name)
    {

        if (isset($this->getRelations()[$name])){
            if ($this->getRelations()[$name]['type']==self::ONE_TO_MANY) {
                return $this->getOneToMany($this->getRelations()[$name]['target'], $this->getRelations()[$name]['link']);
            } else {
                return $this->getManyToOne($this->getRelations()[$name]['target'], $this->getRelations()[$name]['link']);
            }
        } else {
            return 'Nu exista relatia '.static::class."->".$name;
        }
    }




    public function getOneToMany($className, $link)
    {
        $data = dbSelect($className::getTable(),[$link=>$this->getId()]);
        $result = [];
        foreach ($data as $productData){
            $result[]=new $className($productData['id']);
        }

        return $result;
    }



    public function getManyToOne($className, $link)
    {
        if ($this->$link){
            return new $className($this->$link);
        }
        return null;
    }



    public static function getTable()
    {
        return strtolower(static::class);
    }


    public function delete()
    {
        dbDelete(static::getTable(), $this->id);
    }


    public function save()
    {
        if (!is_null($this->id)) {
            dbUpdate(static::getTable(), $this->id, get_object_vars($this));
        } else {
            $this->id = dbInsert(static::getTable(), get_object_vars($this));
        }
    }

    public static function findAll()
    {
        return self::findBy([]);
    }

    public static function findBy($filters)
    {
        $data = dbSelect(static::getTable(),$filters);

        $result = [];
        $className = static::class;
        foreach ($data as $productData){
            $result[]=new $className($productData['id']);
        }

        return $result;

    }

    public static function findOneBy($filters)
    {
        $result = self::findBy($filters);
        if (isset($result[0])) {
            return $result[0];
        }

        return null;
    }

    public static function find($id)
    {
        return self::findOneBy(['id'=>$id]);
    }

}