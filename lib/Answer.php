<?php
 class Answer extends BaseEntity {

    public $answer;

    public $correct;

    public $question_id;

     public function getRelations()
     {
         return [
             'question' =>[
                 'target'=>Question::class,
                 'type' => self::MANY_TO_ONE,
                 'link' => 'question_id'
             ]
         ];
     }
 }