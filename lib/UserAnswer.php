<?php

/**
 * Created by PhpStorm.
 * User: Mihaela
 * Date: 03/09/2019
 * Time: 19:30
 */
class UserAnswer extends BaseEntity
{
    public $user_id;

    public $question_id;

    public $answer_id;

    public $session_id;

    public static function getTable()
    {
        return 'user_answer';
    }


    public function getRelations()
    {
        return [
            'user' => [
                'target'=>User::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'user_id'
            ],
            'question' => [
                'target'=>Question::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'question_id'
            ],
            'answer' => [
                'target'=>Answer::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'answer_id'
            ],
            'session' => [
                'target'=>Session::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'session_id'
            ]
        ];
    }
}