<?php

/**
 * Created by PhpStorm.
 * User: Mihaela
 * Date: 03/09/2019
 * Time: 19:27
 */
class Test extends BaseEntity
{
    public $name;

    public $next_test_id;

    public $level;

    public function getRelations()
    {
        return [
            'sessions' =>[
                'target'=>Session::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'test_id'
            ],
            'questions' =>[
                'target'=>Question::class,
                'type' => self::ONE_TO_MANY,
                'link' => 'test_id'
            ],
            'next' => [
                'target'=>Test::class,
                'type' => self::MANY_TO_ONE,
                'link' => 'next_test_id'
            ]
        ];
    }

    public function userCanTake(User $user)
    {


        $parent = Test::findOneBy(['next_test_id'=>$this->getId()]);

        if ($parent) {
            return ($parent->getScoreForUser($user)/count($this->questions)) > (1/2);
        }

        return true;
    }

    public function getScoreForUser(User $user)
    {
        $sessions = Session::findBy(['test_id'=>$this->getId(), 'user_id'=>$user->getId()]);
        $session = array_pop($sessions);
        $result = 0;
        if ($session) {
            $userAnswers = UserAnswer::findBy(['session_id' => $session->getId(), 'user_id' => $user->getId()]);

            /** @var UserAnswer $userAnswer */
            foreach ($userAnswers as $userAnswer) {
                if ($userAnswer->answer->correct == 1) {
                    $result++;
                }
            }
        }

        return $result;
    }
}