<?php
require_once "BaseEntity.php";

 class Question extends BaseEntity
{
    public $quiz;

    public $test_id;


     public function getRelations()
     {
         return [
             'answers' =>[
                 'target'=>Answer::class,
                 'type' => self::ONE_TO_MANY,
                 'link' => 'question_id'
             ],
             'test' => [
                 'target'=>Test::class,
                 'type' => self::MANY_TO_ONE,
                 'link' => 'test_id'
             ]
         ];
     }
 }
